import styled from 'styled-components';
import { Box } from 'rebass';

export const ContentContainer = styled(Box)`
  margin: 0 auto;
  max-width: 1600px;
  padding: 1rem;
`;
