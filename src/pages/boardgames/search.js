import React from 'react';

import Layout from '../../components/Layout';
import SearchGame from '../../components/SearchGame';

export default () => {
  return (
    <Layout>
      <SearchGame />
    </Layout>
  );
};
